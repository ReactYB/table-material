import axios from "axios";

const TableTest = axios.create({
  baseURL: "http://192.168.1.150/breaktool/api",
  delayed: true,
});
const country = axios.create({
  baseURL: "https://restcountries.eu/rest/v2/all",
  delayed: true,
});

async function getUsers() {
  return await TableTest({
    method: "post",
    url: "/user/getAdminData",
  });
}
async function getUser(id) {
  return await TableTest({
    method: "post",
    url: "/user/globalData?id=" + id,
  });
}
async function getCountry() {
  return await country({
    method: "get",
  });
}
export { getUsers, getUser, getCountry };
