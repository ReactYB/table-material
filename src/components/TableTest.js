import React, { useState } from "react";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import { getCountry, getUser, getUsers } from "../tools/axios";
import { useQuery } from "react-query";
import Loading from "./Loading";
import Erreur from "./Error/Erreur";
import "./style/style.css";
import { Tableee } from "./Tableee/Tableee";
import  Country  from "./Country";

const columns = [
  { id: "id", label: "id", minWidth: 50 },
  { id: "label", label: "Nom", minWidth: 100 },
  {
    id: "max",
    label: "Max",
    minWidth: 170,
  },
  {
    id: "isOpen",
    label: "Ouvert",
    minWidth: 170,
  },
];

const TableTest = () => {
  const [smile, setSmile] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(2);
  ////////////////////////
  //////////////////////
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
const DEF = useQuery(
  "DEF",
 () => {
    return getCountry().then((e) => e);
  },
  {
    enabled:!!smile
  }
);
  const ABC =  useQuery(
    "ABC",
   async () => {
       return [
         await getUsers().then((e) => e), //data[0]
         await getUser(64).then((e) => e),
       ];
    },
    {
      refetchInterval: 10000,
    }
  );
  if (ABC.error ) return <Erreur />;

  return (
    <div className="TableTest">
      {ABC.isLoading ? (
        <Loading />
      ) : (
        <>
          {console.log(ABC)}
          {console.log(DEF)}
          <Paper className="root">
            <TableContainer className="container">
              <Table
                stickyHeader
                className="TBMate"
                aria-label="customized table"
              >
                <TableHead>
                  <TableRow>
                    {columns.map((column) => (
                      <TableCell
                        key={column.id}
                        align="center"
                        style={{ minWidth: column.minWidth }}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {ABC.data[0].data.data
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.code}
                        >
                          {columns.map((column) => {
                            const value = row.group[column.id];
                            return (
                              <TableCell key={column.id} align="center">
                                {column.format && typeof value === "number"
                                  ? column.format(value)
                                  : value}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[2, 4, 8, 10]}
              component="div"
              count={ABC.data[0].data.data.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </Paper>
          <button onClick={() => setSmile(true)} >
            {" "}
            Smile{" "}
          </button>
          <Tableee DEF={ABC} />
          {smile ? <Country country={DEF} /> : ""}
        </>
      )}
    </div>
  );
};

export default TableTest;
