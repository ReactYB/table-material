import React from 'react'
import './style/style.css'
const Loading = () => {
    return (
      <div id="outer">
        <div id="middle">
          <div id="inner"></div>
        </div>
      </div>
    );
}

export default Loading
