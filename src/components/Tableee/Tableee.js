import React from 'react'
import MaterialTable from "material-table";
import Loading from '../Loading';
import './style.css'


export const Tableee = ({DEF}) => {


    return (
      <div className="TableTest" >
        {DEF.data[1].isLoading ? (
          <Loading />
        ) : (
          <MaterialTable
            title="Liste d'attente"
            data={DEF.data[1].data.pauses.filter((e) => e.etat === 0)}
            columns={[
              {
                title: "Ranking",
                field: "ranking",
                hiddenByColumnsButton: "true",
                width: "10% !important",
                cellStyle: {
                  backgroundColor: "#e6e6ff",
                  textAlign: "center",
                },
                // defaultGroupSort:'asc'
              },
              {
                title: "Pseudo",
                field: "pseudo",
                cellStyle: {
                  textAlign: "center",
                },
              },
            ]}
            localization={{
              body: {
                emptyDataSourceMessage: "Aucun agent en attente.",
              },
            }}
            style={{
              width: "100%",
              boxShadow: "0px 0px 15px 6px rgba(0,0,0,0.1)",
              borderRadius: "20px",
            }}
            options={{
              draggable: false,
              headerStyle: {
                backgroundColor: "#154c79",
                color: "#ffffff",
                textAlign: "center",
                fontWeight: "bold",
                position: "sticky",
              },
              maxBodyHeight: "300px",
              minBodyHeight: "300px",
              rowStyle: (rowData) => {
                // console.log(rowData)
                if (rowData.notif === 1) {
                  return { color: "#ff0000" };
                } else if (rowData.awaiting === 1) {
                  return { color: "#DAA520" };
                }
                return { color: "#006400" };
              },
              search: false,
              paging: false,
              // exportButton: true,
              // exportAllData: true,
            }}
          />
        )}
      </div>
    );
}
